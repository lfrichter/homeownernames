<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Traits\ImportCsvTrait;
use Illuminate\Http\Request;

class HomeownerController extends Controller
{

    use ImportCsvTrait;

    /**
     * @param Request $request
     * @return mixed
     */
    public function importCsv(Request $request)
    {
        $message = 'Homeowners had some error during import!';


        if($this->import()){
            $message = 'Homeowners were imported successfully!';
        }

        session()->flash('status', $message);

        return response()->json(compact('message'));
    }

}
