<?php

namespace App\Http\Controllers\Traits;

use App\Homeowner;

trait ImportCsvTrait
{
    public $aHomeowners = [];

    /**
     * @return bool
     */
    protected function import()
    {
        try {
            //set the path for the csv files
            $path = base_path('storage/app/homeowners/*.csv');

            foreach (array_slice(glob($path), 0, 2) as $file) {
                //read the data into an array
                $data = array_map('str_getcsv', file($file));

                unset($data[0]);

                //loop over the data
                $i = 1;
                foreach ($data as $key => $row) {
                    unset($data[$key][1]);
                    $el = $row[0];
                    $data[$key][0] = $el;

                    if ($el === 'homeowner') {
                        continue;
                    }

                    $this->checkOccurrence($el, $i);
                    ++$i;
                }
            }

            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());

            return false;
        }
    }

    /**
     * @param $el
     */
    private function checkOccurrence($el, int $i): void
    {
        $homeowner = new Homeowner();
        $runned = 0;
        foreach (Homeowner::$conjunctions as $conjunction) {
            if (strpos($el, $conjunction)) {
                $aPerson = array_reverse(explode($conjunction, $el));

                foreach ($aPerson as $key => $person) {
                    $words = strpos($person, ' ');

                    if ($words) {
                        $homeowner = $this->createHomeowner($person);
                    } else {
                        $this->createHomeowner($person, $homeowner);
                    }
                    $runned = 1;
                }
            }
        }

        if (!$runned) {
            $this->createHomeowner($el);
        }
    }

    private function createHomeowner($el, Homeowner $previousHomeOwner = null)
    {
        $aName = explode(' ', $el);

        foreach ($aName as $name) {
            $title = in_array($name, Homeowner::$titles) ? $name : '';
            if ($title) {
                break;
            }
        }

        // Set data from later when does not exist
        if (!empty($previousHomeOwner)) {
            $initial = $previousHomeOwner->initial ?? '';
            $first_name = $previousHomeOwner->first_name;
            $last_name = $previousHomeOwner->last_name ?? '';
        } else {
            $initial = $this->getInitial($aName);
            $first_name = $this->getFirstName($aName);
            $last_name = $this->getLastName($aName);
        }

        $homeowner = Homeowner::firstOrCreate(compact('title', 'first_name', 'initial', 'last_name'));

        $this->aHomeowners[] = $homeowner;

        return $homeowner;
    }

    /**
     * @return mixed|string
     */
    private function getFirstName(array $names)
    {
        return isset($names[1]) && strlen($names[1]) >= 3 ? $names[1] : '';
    }

    /**
     * @return mixed|string
     */
    private function getInitial(array $names)
    {
        return isset($names[1]) && strlen($names[1]) <= 2 ? $names[1] : '';
    }

    /**
     * @return mixed
     */
    private function getLastName(array $names)
    {
        return isset($names[2]) && strlen($names[2]) >= 2 ? $names[2] : '';
    }
}
