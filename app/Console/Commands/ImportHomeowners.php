<?php

namespace App\Console\Commands;

use App\Http\Controllers\Traits\ImportCsvTrait;
use Illuminate\Console\Command;

class ImportHomeowners extends Command
{
    use ImportCsvTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:homeowners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Home Owners from stored csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $message = 'Homeowners had some error during import!';

        if ($this->import()) {
            $message = 'Homeowners were imported successfully!';

            foreach ($this->aHomeowners as $homeowner) {
                $status = $homeowner->wasRecentlyCreated ? 'created' : 'updated';
                $this->info($homeowner->id.'. '.$homeowner->first_name.' '.$homeowner->last_name.' - '.$status);
            }
        }

        $this->info($message);
    }
}
