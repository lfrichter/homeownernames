<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homeowner extends Model
{
    protected $fillable = ['title', 'first_name', 'initial', 'last_name'];

    public static $titles = [
        'Mr',
        'Ms',
        'Miss',
        'Mrs',
        'Dr',
        'Prof',
        'Mister',
    ];

    public static $conjunctions = [
        ' and ',
        ' & ',
    ];
}
