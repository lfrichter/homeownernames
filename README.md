# Homeowner Names
@(lfrichter's notebook)[job]


## Environment

This application is using **Sqlite** Database, creates file `database\database.sqlite` if needed.

## Tests

Run tests with `.\vendor\bin\phpunit`

## Import 

- The source file of homeowners to be imported is at
`storage\app\homeowners\examples.csv`
- To start import homeowners you can use: 
  - Command `artisan import:homeowners`  
  - Api endpoint `/api/importCsv`

- Sample of table on Sqlite database

![HomeownersSqlite](https://i.imgur.com/XTyPsDf.png)
