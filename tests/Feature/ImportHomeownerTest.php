<?php

namespace Tests\Feature;

use App\Homeowner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ImportHomeownerTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function import_homeowners_from_csv_file_using_endpoint()
    {
        $homeowner = Homeowner::all();
        $this->assertCount(0, $homeowner);

        $this->get('/api/importCsv')
            ->assertSessionHas('status','Homeowners were imported successfully!')
            ->assertStatus(200);

        $homeowner = Homeowner::all();
        $this->assertCount(18, $homeowner);
    }

    /** @test */
    public function import_homeowners_from_csv_file_using_command()
    {

        $this->artisan('import:homeowners')
            ->expectsOutput('Homeowners were imported successfully!')
            ->assertExitCode(0);

    }
}
