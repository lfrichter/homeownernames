<?php

namespace Tests\Unit;

use App\Homeowner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeownerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_homeowner_id_is_recorded()
    {

        Homeowner::create([
            'title' => 'Mr',
            'first_name' => 'John',
            'initial' => 'B',
            'last_name' => 'Smith'
        ]);

        $this->assertCount(1, Homeowner::all());
    }
}
